import React from "react";
import { useSelector } from "react-redux";
import { selectColors } from "../colors/colorSlice";

export const CoolorsLink: React.FC<{} & React.AnchorHTMLAttributes<
  HTMLElement
>> = props => {
  const colors = useSelector(selectColors);
  const colorsInColorScheme = 5;
  const originalColors = colors.slice(-colorsInColorScheme);
  const path =
    originalColors.length === 5
      ? originalColors.map(c => c.hex()).join("-")
      : "";
  return (
    <a
      target="_blank"
      rel="noopener noreferrer"
      href={`https://coolors.co/app/${path}`}
      {...props}
    >
      {props.children}
    </a>
  );
};
