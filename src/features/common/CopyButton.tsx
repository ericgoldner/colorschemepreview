import React, { useState, Fragment } from "react";

import { clipboard } from "../../utils";

import { Button, Snackbar, ButtonProps } from "@material-ui/core";

import Alert from "@material-ui/lab/Alert";

export interface CopyButtonProps extends ButtonProps {
  copyText: string;
}

export const CopyButton: React.FC<React.PropsWithChildren<CopyButtonProps>> = ({
  copyText,
  children,
  ...props
}) => {
  const [copiedText, setCopiedText] = useState<string>("");
  const [copyError, setCopyError] = useState<boolean>(false);
  const performCopy = () => {
    try {
      clipboard.copy(copyText);
      setCopiedText(copyText);
    } catch (e) {
      setCopyError(true);
    }
  };

  const handleAlertClose = (_?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }
    setCopyError(false);
    setCopiedText("");
  };

  return (
    <Fragment>
      <Button
        {...props}
        variant="outlined"
        onClick={performCopy}
        style={{ margin: 5 }}
      >
        <Fragment>{children}</Fragment>
      </Button>
      <Snackbar
        open={!!copiedText}
        autoHideDuration={3000}
        onClose={handleAlertClose}
      >
        <Alert onClose={handleAlertClose} severity="success">
          {/* Prevent flash of empty hex code when copiedText is "" before rerender. */}
          {copiedText && `${copiedText} copied to clipboard.`}
        </Alert>
      </Snackbar>
      <Snackbar
        open={copyError}
        autoHideDuration={3000}
        onClose={handleAlertClose}
      >
        <Alert onClose={handleAlertClose} severity="error">
          There was an error copying your color.
        </Alert>
      </Snackbar>
    </Fragment>
  );
};
