import { Grid, Tooltip } from "@material-ui/core";
import React, { useState } from "react";
import darkImg from "../../img/skyline-dark.jpg";
import lightImg from "../../img/skyline-light.jpg";
import { typedKeys } from "../../utils";
import { Icons } from "../colors/BackgroundPreview";
import Color, { ColorPairing } from "../colors/color";
import { ColorEditStarter, ColorEditStarterProps } from "./ColorEditorStarter";
import { ElementColorEditor } from "./ElementColorEditor";
import {
  StyledButton,
  StyledContainer,
  StyledTypography
} from "./StyledElements";

export interface EditablePreviewStyles {
  header: ColorPairing;
  headerLinks: ColorPairing;
  title: ColorPairing;
  text: ColorPairing;
  button1: ColorPairing;
  button2: ColorPairing;
  altTitle: ColorPairing;
  altText: ColorPairing;
  altButton1: ColorPairing;
  altButton2: ColorPairing;
  footer: ColorPairing;
  footerText: ColorPairing;
}

export interface EditablePreviewProps {}

export const EditablePreview: React.FC<EditablePreviewProps> = props => {
  const [imgType, setImgType] = useState<string>(lightImg);
  const [colors, setColors] = useState({
    body: new ColorPairing(Color.transparent(), Color.white),
    main: new ColorPairing(Color.transparent(), Color.fromString("22252c")),
    title: Color.fromString("3f3250").pairAsBackground(),
    subtitle: Color.fromString("e14658").pairAsBackground(),
    icons: new ColorPairing(Color.fromString("3f3250"), Color.transparent()),
    subcontainer: Color.fromString("ffffff").pairAsBackground(),
    links: new ColorPairing(Color.fromString("2f4fbd"), Color.transparent()),
    button1: Color.fromString("e14658").pairAsBackground(),
    button2: Color.fromString("22252c").pairAsBackground(),
    alt_subcontainer: Color.fromString("22252c").pairAsBackground(),
    alt_links: new ColorPairing(
      Color.fromString("e14658"),
      Color.transparent()
    ),
    alt_button1: Color.fromString("e14658").pairAsBackground(),
    alt_button2: Color.fromString("3f3250").pairAsForeground()
  });
  type ElKey = keyof typeof colors;

  type EditData = {
    key: ElKey;
    colors: ColorPairing;
    calcContrast: (cs: typeof colors) => number;
  };

  const [editData, setEditData] = useState<EditData | null>(null);

  const handleColorPairChange = (pair: ColorPairing | null) => {
    if (editData) {
      setColors(oldColors =>
        Object.assign({}, oldColors, {
          [editData.key]: pair
        })
      );
      setEditData(Object.assign({}, editData, { colors: pair }));
    }
  };

  const createEditParams = (
    key: ElKey,
    calcContrast?: (cs: typeof colors) => number
  ) => {
    return {
      elementKey: key,
      onStartEditing: () =>
        setEditData({
          key,
          colors: colors[key],
          calcContrast: cs =>
            calcContrast ? calcContrast(cs) : cs[key].contrast
        }),
      isEditing: editData?.key === key
    } as ColorEditStarterProps<ElKey>;
  };

  const exportedStyleString = (() => {
    const mappedStyles = typedKeys(colors).reduce((acc, key) => {
      acc["." + key] = {
        color: colors[key].foreground.toString(),
        "background-color": colors[key].background.toString()
      };
      return acc;
    }, {} as any);

    return JSON.stringify(mappedStyles, null, 4).replace(/"/g, "");
  })();

  return (
    <>
      <Grid container direction="row">
        <Grid item xs={2} style={{ padding: "0 8px" }}>
          {editData ? (
            <ElementColorEditor
              allowFg={!editData.colors.foreground.equals(Color.transparent())}
              allowBg={!editData.colors.background.equals(Color.transparent())}
              colors={editData.colors}
              onColorsChanged={handleColorPairChange}
              calcContrast={() => editData.calcContrast(colors)}
            />
          ) : (
            "Select an element to edit its color."
          )}
        </Grid>
        <Grid item xs={10}>
          <ColorEditStarter<ElKey>
            style={{ margin: "auto", textAlign: "center" }}
            {...createEditParams("body")}
          >
            <div style={colors.body.getCssStyle()}>
              <ColorEditStarter<ElKey> {...createEditParams("title")}>
                <StyledTypography variant="h2" colors={colors.title}>
                  Sample header text
                </StyledTypography>
              </ColorEditStarter>

              <ColorEditStarter<ElKey> {...createEditParams("main")}>
                <StyledContainer
                  colors={colors.main}
                  Tag="div"
                  // style={{ padding: 10 }}
                  className="main"
                >
                  <Grid
                    container
                    className="noselect"
                    direction="row"
                    style={{
                      alignItems: "center",
                      justifyItems: "center",
                      alignContent: "center"
                    }}
                  >
                    <Grid item xs={4} style={{ textAlign: "center" }}>
                      <Tooltip title="Click the image to see a darker or lighter one">
                        <img
                          style={{
                            width: "90%",
                            height: "auto",
                            margin: "auto"
                          }}
                          src={imgType}
                          onClick={e => {
                            setImgType(_ =>
                              imgType === lightImg ? darkImg : lightImg
                            );
                            e.stopPropagation();
                          }}
                          alt="Philadelphia skyline"
                        />
                      </Tooltip>
                    </Grid>

                    <Grid item xs={8} style={{ padding: 20 }}>
                      <ColorEditStarter<ElKey>
                        {...createEditParams("subtitle")}
                      >
                        <StyledTypography
                          style={{ textAlign: "center" }}
                          variant="h3"
                          colors={colors.subtitle}
                        >
                          This is a sample subheader
                        </StyledTypography>
                      </ColorEditStarter>

                      <ColorEditStarter<ElKey>
                        {...createEditParams("subcontainer")}
                      >
                        <StyledContainer
                          style={{ padding: 20 }}
                          colors={colors.subcontainer}
                          Tag="div"
                          // style={{ padding: 10 }}
                          className="alt-inner"
                        >
                          <ColorEditStarter<ElKey>
                            {...createEditParams("icons", cs => {
                              return cs.icons.foreground.contrastWith(
                                cs.subcontainer.background
                              );
                            })}
                          >
                            <div style={{ textAlign: "center" }}>
                              <Icons color={colors.icons.foreground} />
                            </div>
                          </ColorEditStarter>

                          <ColorEditStarter<ElKey>
                            {...createEditParams("links", cs =>
                              cs.links.foreground.contrastWith(
                                cs.subcontainer.background
                              )
                            )}
                          >
                            <span
                              style={{
                                textDecoration: "underline",
                                cursor: "pointer",
                                color: colors.links.foreground.toString(),
                                fontWeight: "bold"
                              }}
                            >
                              This is a sample link.
                            </span>
                          </ColorEditStarter>

                          <StyledTypography
                            variant="body1"
                            colors={colors.subcontainer}
                            style={{
                              padding: 40,
                              fontSize: "1.3em",
                              fontWeight: "bold"
                            }}
                          >
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Perspiciatis impedit, itaque quaerat libero
                            eaque rerum eveniet perferendis est assumenda
                            voluptas, recusandae quos nesciunt culpa consequatur
                            labore blanditiis saepe, distinctio similique!
                          </StyledTypography>

                          <div style={{ textAlign: "center" }}>
                            <ColorEditStarter<ElKey>
                              {...createEditParams("button1")}
                            >
                              <StyledButton
                                style={{ margin: 10 }}
                                variant="contained"
                                size="large"
                                colors={colors.button1}
                              >
                                Button One
                              </StyledButton>
                            </ColorEditStarter>

                            <ColorEditStarter<ElKey>
                              {...createEditParams("button2")}
                            >
                              <StyledButton
                                style={{ margin: 10 }}
                                variant="contained"
                                size="large"
                                colors={colors.button2}
                              >
                                Button Two
                              </StyledButton>
                            </ColorEditStarter>
                          </div>
                        </StyledContainer>
                      </ColorEditStarter>

                      <ColorEditStarter<ElKey>
                        {...createEditParams("alt_subcontainer")}
                      >
                        <StyledContainer
                          style={{ padding: 20 }}
                          colors={colors.alt_subcontainer}
                          Tag="div"
                          // style={{ padding: 10 }}
                          className="alt-inner"
                        >
                          <ColorEditStarter<ElKey>
                            {...createEditParams("alt_links", cs =>
                              cs.alt_links.foreground.contrastWith(
                                cs.alt_subcontainer.background
                              )
                            )}
                          >
                            <span
                              style={{
                                textDecoration: "underline",
                                cursor: "pointer",
                                color: colors.alt_links.foreground.toString(),
                                fontWeight: "bold"
                              }}
                            >
                              This is a sample link.
                            </span>
                          </ColorEditStarter>

                          <StyledTypography
                            variant="body1"
                            colors={colors.alt_subcontainer}
                            style={{
                              padding: 40,
                              fontSize: "1.3em",
                              fontWeight: "bold"
                            }}
                          >
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Perspiciatis impedit, itaque quaerat libero
                            eaque rerum eveniet perferendis est assumenda
                            voluptas, recusandae quos nesciunt culpa consequatur
                            labore blanditiis saepe, distinctio similique!
                          </StyledTypography>
                          <div style={{ textAlign: "center" }}>
                            <ColorEditStarter<ElKey>
                              {...createEditParams("alt_button1")}
                            >
                              <StyledButton
                                style={{ margin: 10 }}
                                variant="contained"
                                size="large"
                                colors={colors.alt_button1}
                              >
                                Button One
                              </StyledButton>
                            </ColorEditStarter>
                            <ColorEditStarter<ElKey>
                              {...createEditParams("alt_button2")}
                            >
                              <StyledButton
                                style={{ margin: 10 }}
                                variant="contained"
                                size="large"
                                colors={colors.alt_button2}
                              >
                                Button Two
                              </StyledButton>
                            </ColorEditStarter>
                          </div>
                        </StyledContainer>
                      </ColorEditStarter>
                    </Grid>
                  </Grid>
                </StyledContainer>
              </ColorEditStarter>
            </div>
          </ColorEditStarter>
        </Grid>
      </Grid>
      <textarea
        readOnly
        value={exportedStyleString}
        style={{ width: "100%", height: "400px" }}
      ></textarea>
    </>
  );
};
