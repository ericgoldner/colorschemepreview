import { IconButton, Typography, Grid } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import React, { ReactNode } from "react";
import { ColorResult, SketchPicker } from "react-color";
import { useDispatch, useSelector } from "react-redux";
import { ColorBlock } from "../colors/BackgroundPreview";
import Color from "../colors/color";

import {
  addCustomColor,
  removeCustomColor,
  selectRawColors,
  selectRawCustomColors
} from "../colors/colorSlice";
import { CopyButton } from "../common/CopyButton";

export interface ColorEditorPickerProps {
  label: ReactNode;
  color: string;
  onChange: (hex: string) => void;
}
export const ColorEditorPicker: React.FC<ColorEditorPickerProps> = ({
  label,
  color,
  onChange
}) => {
  const schemeColors = useSelector(selectRawColors);
  const customColors = useSelector(selectRawCustomColors);
  const dispatch = useDispatch();

  const availableColors = [
    ...new Set(schemeColors.map(c => `#${c}`).concat(customColors))
  ];

  const handleCustomColorToggle = (hex: string) => {
    const normalizedHex = Color.fromString(hex).toString();
    const action = customColors.includes(normalizedHex)
      ? removeCustomColor(normalizedHex)
      : addCustomColor(normalizedHex);
    dispatch(action);
  };

  const handleChange = (s: ColorResult) => {
    onChange(s.hex);
  };

  return (
    <>
      <Grid
        container
        direction="row"
        style={{ alignItems: "center", justifyContent: "space-between" }}
      >
        <Grid item xs={3}>
          {label}
        </Grid>
        <Grid item>
          <CopyButton
            startIcon={<FileCopyIcon />}
            copyText={color}
            endIcon={<ColorBlock size={20} blockColor={Color.fromHex(color)} />}
          >
            <Typography
              style={{ fontFamily: '"Courier New",Courier,monospace' }}
            >
              {color}
            </Typography>
          </CopyButton>
        </Grid>

        <Grid item xs={2}>
          {!schemeColors.includes(color.substring(1)) && (
            <IconButton
              aria-label="update-custom-palette"
              size="small"
              onClick={() => handleCustomColorToggle(color)}
            >
              {customColors.includes(color) ? (
                <DeleteOutlineIcon />
              ) : (
                <AddIcon />
              )}
            </IconButton>
          )}
        </Grid>
      </Grid>
      <div style={{ margin: "auto" }} className="noselect">
        <SketchPicker
          width="90%"
          color={color}
          disableAlpha={true}
          presetColors={availableColors}
          onChange={handleChange}
        />
      </div>
    </>
  );
};
