import { makeStyles } from "@material-ui/core";

import { green } from "@material-ui/core/colors";

import React from "react";

const useHoverStyles = makeStyles(theme => ({
  outline: {
    border: "3px solid transparent",
    pointer: "hand",
    "&:hover": { border: "3px solid red", pointer: "hand" }
  },
  outlinable: {
    border: "3px solid transparent",
    pointer: "hand"
  },
  currentlyEdited: {
    border: "3px dashed blue",
    pointer: "hand"
  },
  toggleFab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2)
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    "&:hover": {
      backgroundColor: green[600]
    }
  }
}));

export interface ColorEditStarterProps<TKey> extends React.HTMLAttributes<{}> {
  elementKey: TKey;
  isEditing?: boolean;
  children?: any;
  onStartEditing: (elementKey: TKey) => void;
}

export function ColorEditStarter<TKey>({
  elementKey,
  isEditing,
  onStartEditing,
  children,
  ...props
}: ColorEditStarterProps<TKey>) {
  const handleStart = (event: React.MouseEvent<{}, MouseEvent>) => {
    onStartEditing(elementKey);
    event.stopPropagation();
  };
  const classes = useHoverStyles();

  return React.Children.only(
    React.cloneElement(children, {
      className: `${classes.outlinable} ${
        isEditing ? classes.currentlyEdited : classes.outline
      } ${props.className}`,
      onClick: handleStart
    } as React.HTMLAttributes<{}>)
  );
}

ColorEditStarter.defaultProps = {
  isEditing: false
};
