import {
  makeStyles,
  ButtonProps,
  Button,
  TypographyProps,
  Typography
} from "@material-ui/core";

import { ColorPairing } from "../colors/color";
import React from "react";

const useColorStyles = makeStyles({
  root: ({ colors }: { colors: ColorPairing }) => colors.getCssStyle()
});

export const StyledButton = ({
  colors,
  ...props
}: { colors: ColorPairing } & ButtonProps) => {
  const classes = useColorStyles({ colors });
  return (
    <Button {...props} className={props.className + " " + classes.root}>
      {props.children}
    </Button>
  );
};

export const StyledTypography = ({
  colors,
  ...props
}: { colors: ColorPairing } & TypographyProps) => {
  const classes = useColorStyles({ colors });
  return (
    <Typography {...props} className={props.className + " " + classes.root}>
      {props.children}
    </Typography>
  );
};

export const StyledContainer = ({
  Tag: tag,
  colors,
  ...props
}: {
  Tag: keyof JSX.IntrinsicElements;
  colors: ColorPairing;
} & React.HTMLAttributes<HTMLOrSVGElement>) => {
  const colorClasses = useColorStyles({ colors });
  const className = (props.className || "") + " " + colorClasses.root;
  const Tag = tag;
  return (
    <Tag {...props} className={className}>
      {props.children}
    </Tag>
  );
};
