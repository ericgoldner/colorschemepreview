import Color, { ColorPairing, ColorLayer } from "../colors/color";

import {
  makeStyles,
  Popper,
  ClickAwayListener,
  Grid,
  Typography,
  Button,
  Snackbar,
  IconButton
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";

import React, { FC, useState, useEffect } from "react";

import { ColorResult, SketchPicker } from "react-color";

import { useSelector, useDispatch } from "react-redux";

import {
  selectRawColors,
  selectRawCustomColors,
  addCustomColor,
  removeCustomColor
} from "../colors/colorSlice";

import { clipboard } from "../../utils";

import Alert from "@material-ui/lab/Alert";

interface HoverTargetProps extends React.HTMLAttributes<{}> {
  enableFg?: boolean;
  enableBg?: boolean;
  colors: ColorPairing;
  colorChangeEnabled?: boolean;
  children?: any;
  onColorsChanged: (colors: ColorPairing) => void;
}

const useHoverStyles = makeStyles({
  outline: {
    border: "3px solid transparent",
    pointer: "hand",
    "&:hover": { border: "3px solid red" }
  },
  outlinable: {
    border: "3px solid transparent",
    pointer: "hand"
  }
});

/** Adds a hover notification and displays a  */
export const ChildColorEditor: FC<HoverTargetProps> = ({
  enableFg,
  enableBg,
  colors,
  colorChangeEnabled,
  onColorsChanged,
  children,
  ...props
}) => {
  colorChangeEnabled = colorChangeEnabled ?? true;
  const classes = useHoverStyles();
  const schemeColors = useSelector(selectRawColors);
  const customColors = useSelector(selectRawCustomColors);
  const dispatch = useDispatch();

  const [showPicker, setShowPicker] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const [copiedText, setCopiedText] = useState<string>("");
  const [copyError, setCopyError] = useState<boolean>(false);
  const handleClick = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    e.stopPropagation();
    setAnchorEl(e.currentTarget);
    setShowPicker(!showPicker);
  };

  const handleChange = (c: ColorResult | Color, layer: ColorLayer) => {
    let hex: string = c instanceof Color ? c.hex() : c.hex;
    const color = Color.fromString(hex);
    const fg = layer === ColorLayer.fg ? color : colors.foreground;
    const bg = layer === ColorLayer.bg ? color : colors.background;
    onColorsChanged(new ColorPairing(fg, bg));
  };

  const handleClose = () => {
    //setAnchorEl(null);
    setShowPicker(false);
  };

  const renderChildren = () => {
    const showOutline = !showPicker && colorChangeEnabled;
    return React.Children.only(
      React.cloneElement(children, {
        className: `${classes.outlinable} ${
          showOutline ? classes.outline : ""
        } ${props.className}`,
        onClick: handleClick
      })
    );
  };

  const handleCustomColorToggle = (hex: string) => {
    const normalizedHex = Color.fromString(hex).toString();
    const action = customColors.includes(normalizedHex)
      ? removeCustomColor(normalizedHex)
      : addCustomColor(normalizedHex);
    dispatch(action);
  };

  // const colorScheme = useSelector(selectRawColorList);
  // const webColorScheme = colorScheme.length
  //   ? colorScheme.map(c => "#" + c)
  //   : undefined;

  /* Firefox selection goes haywire while editing colors without this method */
  const preventPropagation = (
    event:
      | React.MouseEvent<HTMLElement, MouseEvent>
      | React.TouchEvent<HTMLElement>
  ) => {
    event.stopPropagation();
    event.preventDefault();
  };

  const copyText = (data: string) => {
    try {
      clipboard.copy(data);
      setCopiedText(data);
    } catch (e) {
      setCopyError(true);
    }
  };

  const handleAlertClose = (_?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }
    setCopyError(false);
    setCopiedText("");
  };

  const availableColors = [
    ...new Set(schemeColors.map(c => `#${c}`).concat(customColors))
  ];

  return (
    <>
      {renderChildren()}

      <Popper
        open={showPicker}
        anchorEl={anchorEl}
        style={{ zIndex: 9999 }}
        placement="bottom"
        //onMouseDown={preventPropagation}
        onClick={preventPropagation}
        modifiers={{
          flip: {
            enabled: true
          },
          preventOverflow: {
            enabled: true,
            boundariesElement: "scrollParent"
          }
        }}
      >
        <ClickAwayListener onClickAway={handleClose}>
          <Grid
            container
            direction="row"
            style={{
              padding: 50,
              background: "white",
              border: "2px solid black",
              borderRadius: 20
            }}
          >
            {enableBg && (
              <Grid item>
                <Typography variant="h5">Background:</Typography>
                <SketchPicker
                  presetColors={availableColors}
                  disableAlpha={true}
                  color={colors.background.hex()}
                  onChange={c => handleChange(c, ColorLayer.bg)}
                />
                <Button
                  variant="outlined"
                  onClick={() => copyText(colors.background.toString())}
                  style={{ margin: 5 }}
                >
                  Copy {colors.background.toString()}
                </Button>

                <IconButton
                  aria-label="update-custom-palette"
                  onClick={() =>
                    handleCustomColorToggle(colors.background.toString())
                  }
                >
                  {customColors.includes(colors.background.toString()) ? (
                    <DeleteOutlineIcon />
                  ) : (
                    <AddIcon />
                  )}
                </IconButton>
              </Grid>
            )}
            {enableFg && (
              <Grid item>
                <Typography variant="h5">Foreground:</Typography>
                <SketchPicker
                  presetColors={availableColors}
                  disableAlpha={true}
                  color={colors.foreground.hex()}
                  onChange={c => handleChange(c, ColorLayer.fg)}
                />
                <Button
                  variant="outlined"
                  onClick={() => copyText(colors.foreground.toString())}
                  style={{ margin: 5 }}
                >
                  Copy {colors.foreground.toString()}
                </Button>
                <IconButton
                  aria-label="update-custom-palette"
                  onClick={() =>
                    handleCustomColorToggle(colors.foreground.toString())
                  }
                >
                  {customColors.includes(colors.foreground.toString()) ? (
                    <DeleteOutlineIcon />
                  ) : (
                    <AddIcon />
                  )}
                </IconButton>
              </Grid>
            )}
          </Grid>
        </ClickAwayListener>
      </Popper>
      <Snackbar
        open={!!copiedText}
        autoHideDuration={3000}
        onClose={handleAlertClose}
      >
        <Alert onClose={handleAlertClose} severity="success">
          {/* Prevent flash of empty hex code when copiedText is "" before rerender. */}
          {copiedText && `${copiedText} copied to clipboard.`}
        </Alert>
      </Snackbar>
      <Snackbar
        open={copyError}
        autoHideDuration={3000}
        onClose={handleAlertClose}
      >
        <Alert onClose={handleAlertClose} severity="error">
          There was an error copying your color.
        </Alert>
      </Snackbar>
    </>
  );
};

ChildColorEditor.defaultProps = {
  enableFg: true,
  enableBg: true
};
