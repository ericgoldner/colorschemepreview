import { Divider, Typography } from "@material-ui/core";
import React from "react";
import Color, { ColorLayer, ColorPairing } from "../colors/color";
import {
  A11yLevel,
  largeTextA11yCalculator,
  smallTextA11yCalculator
} from "../filters/a11y";
import { ColorEditorPicker } from "./ColorEditorPicker";
import { StyledTypography } from "./StyledElements";

export interface ElementColorEditorProps {
  colors: ColorPairing;
  allowFg?: boolean;
  allowBg?: boolean;
  onColorsChanged: (colors: ColorPairing) => void;
  calcContrast?: () => number;
}

export const ElementColorEditor: React.FC<ElementColorEditorProps> = ({
  colors,
  allowFg,
  allowBg,
  onColorsChanged,
  calcContrast
}) => {
  const handleChange = (hex: string, layer: ColorLayer) => {
    const color = Color.fromString(hex);
    const fg = layer === ColorLayer.fg ? color : colors.foreground;
    const bg = layer === ColorLayer.bg ? color : colors.background;
    onColorsChanged(new ColorPairing(fg, bg));
  };

  const contrast = calcContrast ? calcContrast() : colors.contrast;

  const contrastLabel = (level: A11yLevel) => {
    const colorName =
      level === A11yLevel.aaa
        ? "green"
        : level === A11yLevel.aa
        ? "yellow"
        : "red";
    const colors = Color.fromName(colorName).pairAsBackground();
    return (
      <StyledTypography colors={colors}>
        {A11yLevel[level].toUpperCase()}
      </StyledTypography>
    );
  };

  return (
    <div>
      <table style={{ textAlign: "left", width: "100%" }}>
        <thead>
          <tr>
            <th>Contrast Ratio</th>
            <th>Large</th>
            <th>Small</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <strong>{contrast.toFixed(2)}</strong>
            </td>
            <td>{contrastLabel(largeTextA11yCalculator(contrast))}</td>
            <td>{contrastLabel(smallTextA11yCalculator(contrast))}</td>
          </tr>
        </tbody>
      </table>

      {allowFg && (
        <>
          <ColorEditorPicker
            label={<Typography variant="h5">Text</Typography>}
            onChange={c => handleChange(c, ColorLayer.fg)}
            color={colors.foreground.toString()}
          />
        </>
      )}
      <Divider variant="middle" style={{ margin: "10px 0" }} />
      {allowBg && (
        <>
          <ColorEditorPicker
            label={<Typography variant="h5">Back</Typography>}
            onChange={c => handleChange(c, ColorLayer.bg)}
            color={colors.background.toString()}
          />
        </>
      )}
    </div>
  );
};

ElementColorEditor.defaultProps = {
  allowBg: true,
  allowFg: true
};
