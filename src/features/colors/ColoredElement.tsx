import React from "react";
import "./ColoredElement.css";
import Color, { ColorPairing } from "./color";

export interface ColoredElementProps
  extends React.HTMLAttributes<HTMLOrSVGElement> {
  colors: ColorPairing;
  factory?: (colors: ColorPairing) => JSX.Element;
  tag: keyof JSX.IntrinsicElements;
  text: string | ((colors: ColorPairing) => string);
}

function ColoredElement({
  colors,
  factory,
  tag,
  text,
  style,
  children,
  ...props
}: ColoredElementProps) {
  const { foreground: fg, background: bg } = colors;

  if (factory) {
    return factory(colors);
  } else {
    const Tag = tag;
    const label = typeof text === "function" ? text(colors) : text;
    return (
      <Tag
        {...props}
        style={{
          ...style,
          color: fg.toString(),
          backgroundColor: bg.toString()
        }}
      >
        {label}
        {children}
      </Tag>
    );
  }
}

ColoredElement.defaultProps = {
  tag: "div",
  colors: Color.black.pairAsForeground(),
  text: "Text",
  fg: Color.black,
  aa: 0,
  aaa: 0
};

export default ColoredElement;
