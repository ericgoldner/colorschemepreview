import { createSlice, PayloadAction, createSelector } from "@reduxjs/toolkit";
import Color, { ColorPairing } from "./color";
import { AppState } from "../../store/rootReducer";

// STATE
export interface ColorState {
  colors: string[];
  /** The index of the color currently being edited. */
  editingIdx: number;
  showFilters: boolean;
  /** For editable preview */
  customColors: string[];
}

const initialState: ColorState = {
  colors: [],
  editingIdx: -1,
  showFilters: false,
  customColors: [
    "#ffffff",
    "#000000",
    "#5300eb",
    "#004dcf",
    "#1273de",
    "#006b76",
    "#008b02",
    "#fccb00",
    "#db3e00",
    "#b80000"
  ]
};
// END STATE

// SELECTORS

export const selectRawColors = (state: AppState) => state.colors.colors;
export const selectColors = createSelector(
  selectRawColors,
  (colorCodes: string[]) => colorCodes.map(Color.fromHex)
);
export const selectColor = (state: AppState, index: number) =>
  state.colors.colors[index];

export const makeSelectColorPair = () =>
  createSelector(
    (state: AppState, fg: number, _: number) => selectColor(state, fg),
    (state: AppState, _: number, bg: number) => selectColor(state, bg),
    (fg, bg) => new ColorPairing(Color.fromHex(fg), Color.fromHex(bg))
  );

export const selectNumColors = (state: AppState) =>
  selectRawColors(state).length;
export const selectEditIndex = (state: AppState) => state.colors.editingIdx;
export const selectShowFilters = (state: AppState) => {
  return state.colors.showFilters;
};
export const selectRawCustomColors = (state: AppState) =>
  state.colors.customColors;

export type SetColorPayload<T> = {
  index: number;
  color: T;
};

const colorSlice = createSlice({
  name: "colors",
  initialState,
  reducers: {
    setColors: {
      reducer: (state: ColorState, action: PayloadAction<string[]>) => {
        state.colors = action.payload;
        state.customColors = [];
      },
      prepare: (origPayload: Color[]) => {
        const payload: string[] = origPayload.map(c => c.hex());
        return { payload };
      }
    },

    setColor: {
      reducer: (
        state: ColorState,
        action: PayloadAction<SetColorPayload<string>>
      ) => {
        const { color, index } = action.payload;
        state.colors[index] = color;
      },
      prepare: (origPayload: SetColorPayload<Color>) => {
        const { color, index } = origPayload;
        const payload: SetColorPayload<string> = {
          color: color.hex(),
          index: index
        };
        return { payload };
      }
    },

    setEditColorIndex: (state: ColorState, action: PayloadAction<number>) => {
      state.editingIdx = action.payload;
    },

    toggleFilterDisplay: (
      state: ColorState,
      action: PayloadAction<boolean>
    ) => {
      state.showFilters = action.payload;
    },

    addCustomColor: (state: ColorState, action: PayloadAction<string>) => {
      if (!state.customColors.includes(action.payload)) {
        state.customColors.push(action.payload);
      }
    },

    removeCustomColor: (state: ColorState, action: PayloadAction<string>) => {
      const io = state.customColors.indexOf(action.payload);
      if (io > -1) {
        state.customColors.splice(io, 1);
      }
    }
  }
});

export const {
  setColors,
  setColor,
  setEditColorIndex,
  toggleFilterDisplay,
  addCustomColor,
  removeCustomColor
} = colorSlice.actions;

export default colorSlice.reducer;
