import { Badge, BadgeProps, Fade } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import HomeIcon from "@material-ui/icons/Home";
import SearchIcon from "@material-ui/icons/Search";
import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import { A11yLevel } from "../filters/a11y";
import { selectBgColorVisibilities } from "../filters/filterSlice";
import "./BackgroundPreview.css";
import Color from "./color";
import { selectColors, selectNumColors } from "./colorSlice";
import PairingPreview from "./PairingPreview";

export interface ColorSchemePreviewProps {
  collapseCols: boolean;
}

export const ColorSchemePreview: React.FC<ColorSchemePreviewProps> = ({
  collapseCols
}) => {
  const numColors = useSelector(selectNumColors);
  const visibilities = useSelector(selectBgColorVisibilities);

  return (
    <div style={{ backgroundColor: "#f7f7f7" }}>
      {Array(numColors)
        .fill(0)
        .map((_, i) => {
          return (
            <Fade key={i} in={visibilities[i]} mountOnEnter unmountOnExit>
              <BackgroundPreview collapseCols={collapseCols} bgIndex={i} />
            </Fade>
          );
        })}
    </div>
  );
};

export interface BackgroundPreviewProps {
  bgIndex: number;
  collapseCols: boolean;
}

export const BackgroundPreview: React.FC<BackgroundPreviewProps> = ({
  bgIndex: bgIdx,
  collapseCols
}: BackgroundPreviewProps) => {
  const colors = useSelector(selectColors);
  return (
    <div className="bg-preview">
      {colors.map((fg, fgIdx) => (
        <PairingPreview
          collapseColumns={collapseCols}
          key={fgIdx}
          fgIdx={fgIdx}
          bgIdx={bgIdx}
        ></PairingPreview>
      ))}
    </div>
  );
};

const useBadgeStyles = makeStyles({
  badge: (props: { color: Color }) => ({
    backgroundColor: props.color.toString(),
    color: props.color.defaultText().toString()
  })
});

export const A11yBadge: React.FC<{
  level: A11yLevel;
} & BadgeProps> = ({ level, ...props }) => {
  const label = A11yLevel[level].toUpperCase();
  const color =
    level === A11yLevel.aaa
      ? Color.fromName("green")
      : level === A11yLevel.aa
      ? Color.fromName("yellow")
      : Color.fromName("red");

  const classes = useBadgeStyles({ color });

  return (
    <Badge
      anchorOrigin={{ horizontal: "left", vertical: "top" }}
      badgeContent={label}
      classes={{ badge: classes.badge }}
      {...props}
      color="primary"
    >
      {props.children}
    </Badge>
  );
};

export type ColorBlockProps = {
  blockColor: Color;
  size: number;
  className?: string;
} & React.CSSProperties;

export const ColorBlock: React.FC<ColorBlockProps> = ({
  blockColor,
  size,
  className,
  ...props
}) => {
  return (
    <div
      className={className ?? ""}
      style={{
        backgroundColor: blockColor.toString(),
        width: size,
        height: size,
        display: "inline-block",
        border: "1 px solid black",
        boxShadow: "3px 3px 3px #666",
        ...props
      }}
    >
      &nbsp;
    </div>
  );
};

export const Icons = ({ color }: { color: Color }) => {
  const style = color ? { color: color.toString() } : {};
  return (
    <Fragment>
      <AddShoppingCartIcon fontSize="large" style={style} />
      <SearchIcon fontSize="large" style={style} />
      <HomeIcon fontSize="large" style={style} />
    </Fragment>
  );
};
