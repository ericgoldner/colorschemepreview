export type RGB = [number, number, number];
export type ColorInitializer = string | RGB | number;

export default class Color {
  public readonly value: number;

  public static fromString(str: string) {
    const methods = [Color.fromHex, Color.fromName];
    for (let m of methods) {
      try {
        return m(str);
      } catch (e) {}
    }
    throw new Error("Invalid color string.");
  }

  public static fromNumber(num: number) {
    return new Color(num);
  }

  public static fromHex(val: string) {
    val = val.toLowerCase().replace("#", "");
    if (val.length === 3) {
      val = val[0] + val[0] + val[1] + val[1] + val[2] + val[2];
    }
    if (!/^[a-f0-9]{6}$/.test(val)) {
      throw new Error(
        "Invalid color string provided. Please use a named color or #<HEX> value."
      );
    }

    const value = Color.hexToDec(val);
    return new Color(value);
  }

  public static fromName(val: string) {
    const namedColorHex = Color.byName(val);
    if (!namedColorHex) {
      throw new Error("Invalid color name provided: " + val);
    }
    let value = Color.hexToDec(namedColorHex);
    return new Color(value);
  }

  public static fromRgb(rgb: [number, number, number]) {
    const value = Color.rgbToNumber(...rgb);
    return new Color(value);
  }

  public static copy(color: Color) {
    return new Color(color.value);
  }

  public applyAlpha(alpha: number) {
    return new ColorWithAlpha(this, alpha);
  }

  protected constructor(val: number) {
    if (val < 0 || val > 0xffffff) {
      throw new Error("Numeric color value is out of range.");
    }
    this.value = val;
  }

  private static rgbToNumber(r: number, g: number, b: number) {
    return (r << 16) + (g << 8) + b;
  }

  private static hexToDec(hex: string): number {
    return parseInt(hex, 16);
  }

  protected static numToHex(num: number) {
    return num.toString(16).padStart(2, "0");
  }

  private static rgbToHex(rgb: RGB) {
    return rgb.map(this.numToHex).join("");
  }

  private static luminanace(r: number, g: number, b: number) {
    var a = [r, g, b].map(function(v: number) {
      v /= 255;
      return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, 2.4);
    });
    return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
  }

  private static contrast(rgb1: RGB, rgb2: RGB) {
    const lum1 = Color.luminanace(...rgb1) + 0.05;
    const lum2 = Color.luminanace(...rgb2) + 0.05;
    return lum1 >= lum2 ? lum1 / lum2 : lum2 / lum1;
  }

  private static byName(name: string) {
    return Color.namedColors()[name] || null;
  }

  /**
   *
   */
  private static namedColors(): { [k: string]: string } {
    // Data obtained from source code of: https://www.w3.org/wiki/CSS/Properties/color/keywords
    // - Tool Used: https://regexr.com/
    // - (?:<td>)([^\s\r\n]+)(?:[\r\n\s]*<\/td>)(?:<td class="c" style="background-color:silver;">#)([A-F0-9]{6})(?:[\r\n\s]+<\/td>)
    // - List -> Output = '"$1": "#$2",\n'
    // - Removed 'Basic Colors' duplicates from start (ending in 'aqua')

    return {
      aliceblue: "f0f8ff",
      antiquewhite: "faebd7",
      aqua: "00ffff",
      aquamarine: "7fffd4",
      azure: "f0ffff",
      beige: "f5f5dc",
      bisque: "ffe4c4",
      black: "000000",
      blanchedalmond: "ffebcd",
      blue: "0000ff",
      blueviolet: "8a2be2",
      brown: "a52a2a",
      burlywood: "deb887",
      cadetblue: "5f9ea0",
      chartreuse: "7fff00",
      chocolate: "d2691e",
      coral: "ff7f50",
      cornflowerblue: "6495ed",
      cornsilk: "fff8dc",
      crimson: "dc143c",
      cyan: "00ffff",
      darkblue: "00008b",
      darkcyan: "008b8b",
      darkgoldenrod: "b8860b",
      darkgray: "a9a9a9",
      darkgreen: "006400",
      darkgrey: "a9a9a9",
      darkkhaki: "bdb76b",
      darkmagenta: "8b008b",
      darkolivegreen: "556b2f",
      darkorange: "ff8c00",
      darkorchid: "9932cc",
      darkred: "8b0000",
      darksalmon: "e9967a",
      darkseagreen: "8fbc8f",
      darkslateblue: "483d8b",
      darkslategray: "2f4f4f",
      darkslategrey: "2f4f4f",
      darkturquoise: "00ced1",
      darkviolet: "9400d3",
      deeppink: "ff1493",
      deepskyblue: "00bfff",
      dimgray: "696969",
      dimgrey: "696969",
      dodgerblue: "1e90ff",
      firebrick: "b22222",
      floralwhite: "fffaf0",
      forestgreen: "228b22",
      fuchsia: "ff00ff",
      gainsboro: "dcdcdc",
      ghostwhite: "f8f8ff",
      gold: "ffd700",
      goldenrod: "daa520",
      gray: "808080",
      green: "008000",
      greenyellow: "adff2f",
      grey: "808080",
      honeydew: "f0fff0",
      hotpink: "ff69b4",
      indianred: "cd5c5c",
      indigo: "4b0082",
      ivory: "fffff0",
      khaki: "f0e68c",
      lavender: "e6e6fa",
      lavenderblush: "fff0f5",
      lawngreen: "7cfc00",
      lemonchiffon: "fffacd",
      lightblue: "add8e6",
      lightcoral: "f08080",
      lightcyan: "e0ffff",
      lightgoldenrodyellow: "fafad2",
      lightgray: "d3d3d3",
      lightgreen: "90ee90",
      lightgrey: "d3d3d3",
      lightpink: "ffb6c1",
      lightsalmon: "ffa07a",
      lightseagreen: "20b2aa",
      lightskyblue: "87cefa",
      lightslategray: "778899",
      lightslategrey: "778899",
      lightsteelblue: "b0c4de",
      lightyellow: "ffffe0",
      lime: "00ff00",
      limegreen: "32cd32",
      linen: "faf0e6",
      magenta: "ff00ff",
      maroon: "800000",
      mediumaquamarine: "66cdaa",
      mediumblue: "0000cd",
      mediumorchid: "ba55d3",
      mediumpurple: "9370db",
      mediumseagreen: "3cb371",
      mediumslateblue: "7b68ee",
      mediumspringgreen: "00fa9a",
      mediumturquoise: "48d1cc",
      mediumvioletred: "c71585",
      midnightblue: "191970",
      mintcream: "f5fffa",
      mistyrose: "ffe4e1",
      moccasin: "ffe4b5",
      navajowhite: "ffdead",
      navy: "000080",
      oldlace: "fdf5e6",
      olive: "808000",
      olivedrab: "6b8e23",
      orange: "ffa500",
      orangered: "ff4500",
      orchid: "da70d6",
      palegoldenrod: "eee8aa",
      palegreen: "98fb98",
      paleturquoise: "afeeee",
      palevioletred: "db7093",
      papayawhip: "ffefd5",
      peachpuff: "ffdab9",
      peru: "cd853f",
      pink: "ffc0cb",
      plum: "dda0dd",
      powderblue: "b0e0e6",
      purple: "800080",
      red: "ff0000",
      rosybrown: "bc8f8f",
      royalblue: "4169e1",
      saddlebrown: "8b4513",
      salmon: "fa8072",
      sandybrown: "f4a460",
      seagreen: "2e8b57",
      seashell: "fff5ee",
      sienna: "a0522d",
      silver: "c0c0c0",
      skyblue: "87ceeb",
      slateblue: "6a5acd",
      slategray: "708090",
      slategrey: "708090",
      snow: "fffafa",
      springgreen: "00ff7f",
      steelblue: "4682b4",
      tan: "d2b48c",
      teal: "008080",
      thistle: "d8bfd8",
      tomato: "ff6347",
      turquoise: "40e0d0",
      violet: "ee82ee",
      wheat: "f5deb3",
      white: "ffffff",
      whitesmoke: "f5f5f5",
      yellow: "ffff00",
      yellowgreen: "9acd32"
    };
  }

  toString() {
    return `#${this.hex()}`;
  }

  public rgb(): RGB {
    return [this.r(), this.g(), this.b()];
  }

  equals(otherColor: Color) {
    // Subclass of color
    if (Object.getPrototypeOf(otherColor) !== Color.prototype) {
      return false;
    }
    return this.value === otherColor?.value;
  }

  hex() {
    return Color.rgbToHex(this.rgb());
  }

  r() {
    return (this.value >> 16) & 0xff;
  }

  g() {
    return (this.value >> 8) & 0xff;
  }

  b() {
    return this.value & 0xff;
  }

  namedColor() {
    const colors = Color.namedColors();
    const hex = this.hex();
    const matches = Object.entries(colors).filter(([k, v]) => k === hex);
    return matches.length ? matches[0][1] : "";
  }

  contrastWith(otherColor: Color) {
    return Color.contrast(this.rgb(), otherColor.rgb());
  }

  luminance() {
    return Color.luminanace(...this.rgb());
  }

  defaultText() {
    return this.contrastWith(Color.black) >= this.contrastWith(Color.white)
      ? Color.black
      : Color.white;
  }

  /**
   * Creates a pairing with the most readable foreground.
   */
  pairAsBackground() {
    return new ColorPairing(this.defaultText(), this);
  }

  /**
   * Creates a pairing with the optimum background.
   */
  pairAsForeground() {
    return new ColorPairing(this, this.defaultText());
  }

  static readonly white = Color.fromNumber(0xffffff);
  static readonly black = Color.fromNumber(0x000000);
  static transparent() {
    return Color.white.applyAlpha(0);
  }
}

export class ColorWithAlpha extends Color {
  constructor(public readonly color: Color, public readonly alpha: number) {
    super(color.value);

    if (alpha < 0 || alpha > 0xff) {
      throw new Error("Invalid alphia value.");
    }
  }

  equals(otherColor: Color): boolean {
    if (!(otherColor instanceof ColorWithAlpha)) {
      return false;
    }

    return this.alpha === otherColor.alpha && this.value === otherColor.value;
  }

  hex() {
    return `${super.hex()}${Color.numToHex(this.alpha)}`;
  }

  toString() {
    return `#${this.hex()}`;
  }
}

export enum ColorLayer {
  fg,
  bg
}
export type WebColor = Color | ColorWithAlpha;
export class ColorPairing {
  public readonly contrast: number;

  constructor(
    public readonly foreground: WebColor,
    public readonly background: WebColor
  ) {
    this.contrast = foreground.contrastWith(background);
  }

  getCssStyle() {
    return {
      color: this.foreground.toString(),
      backgroundColor: this.background.toString()
    };
  }

  toString() {
    return `${this.foreground} on ${this.background}`;
  }

  equals(pair: ColorPairing) {
    return (
      this.background.equals(pair.background) &&
      this.foreground.equals(pair.foreground)
    );
  }
}
