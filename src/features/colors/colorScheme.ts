import Color from "./color";
import { filledArray } from "../../utils";

export const parseScheme = (rawText: string) => {
  rawText = rawText?.trim().toLowerCase() ?? "";

  const toColor = (t: string) => Color.fromString(t);

  const matchPattern = (pattern: RegExp, resultIndex: number = 1) => {
    let match: RegExpExecArray | null;
    const colors: Color[] = [];
    while ((match = pattern.exec(rawText ?? "")) !== null) {
      colors.push(toColor(match[1]));
    }
    return colors.length ? colors : null;
  };

  const parsers = [
    {
      name: "Coolor",
      parse: () =>
        /^https?:\/\/coolors\.co\//.test(rawText)
          ? matchPattern(/(?:\/|-)\b([a-f0-9]{6})\b/g, 1)
          : null
    },
    {
      name: "Adobe XML",
      parse: () => matchPattern(/(?:<color .*? rgb=["'])([a-f0-9]{6})/g, 1)
    },
    {
      name: "List of Colors",
      parse: () => matchPattern(/(\b#?[a-f0-9]{6}\b)/g, 1)
    }
  ];

  for (let p of parsers) {
    let colors = p.parse();
    if (colors) {
      return { source: p.name, colors };
    }
  }
  return null;
};

export const normalizeColorScheme = (
  requestedColors: Color[],
  defaultColors: Color[] = []
): Color[] => {
  // TODO: Push to props/options
  const requiredColors = 5;
  const defaultUnusedColor = Color.white;
  // END TODO

  requestedColors = requestedColors.slice(0, requiredColors);
  const shortfall = requiredColors - requestedColors.length;
  const placeholders = filledArray(defaultUnusedColor, shortfall);
  return [...defaultColors, ...requestedColors, ...placeholders];
};
