import { makeStyles, SwitchProps, Switch } from "@material-ui/core";
import Color from "./color";
import React from "react";

const useSwitchStyles = makeStyles({
  switchBase: ({ color }: { color: string }) => ({
    color: color,
    "&$checked": {
      color: color
    },
    "&$checked + $track": {
      backgroundColor: color
    }
  }),
  checked: ({ color }: { color: string }) => ({
    color: color,
    "&$checked": {
      color: color
    },
    "&$checked + $track": {
      backgroundColor: color
    }
  }),
  track: ({ color }: { color: string }) => ({
    color: color
  })
});

export interface ColoredSwitchProps extends SwitchProps {
  customColor: Color;
}
export const ColoredSwitch: React.FC<ColoredSwitchProps> = ({
  customColor,
  ...props
}) => {
  const classes = useSwitchStyles({ color: customColor.toString() });
  return <Switch classes={classes} {...props} />;
};
