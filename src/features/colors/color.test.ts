import produce from "immer";
import Color from "./color";

describe("color", () => {
  it("should parse white hex", () => {
    expect(Color.fromHex("FFFFFF").rgb()).toEqual([255, 255, 255]);
  });

  it("should parse black hex", () => {
    expect(Color.fromHex("000000").rgb()).toEqual([0, 0, 0]);
  });
});
