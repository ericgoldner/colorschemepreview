import { Button, CircularProgress, Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { CoolorsLink } from "../common/CoolorsLink";
import Color from "./color";
import { normalizeColorScheme, parseScheme } from "./colorScheme";
import { selectRawColors } from "./colorSlice";

const ColorSchemeImporter: React.FC<{}> = () => {
  const [currentColors, setCurrentColors] = useState<Color[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const colors = useSelector(selectRawColors);
  const [rawInput, setRawInput] = useState("");

  const updateColor = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const text = event.target.value;
    setRawInput(text);
    const scheme = parseScheme(text);
    if (scheme) {
      const normalizedColors = normalizeColorScheme(scheme.colors);
      setCurrentColors(normalizedColors);
    }
  };

  const previewElements = (
    <div
      style={{
        margin: "0 8px",
        display: "inline-flex",
        height: 30,
        flexDirection: "row",
        alignItems: "stretch"
      }}
    >
      {currentColors.map((c, i) => (
        <div
          key={`${i}${c}`}
          style={{
            width: 30,
            backgroundColor: c.toString(),
            color: c.toString()
          }}
        >
          {" "}
        </div>
      ))}
    </div>
  );

  useEffect(() => {
    if (loading) {
      window.location.href = `?colors=${currentColors
        .map(c => c.hex())
        .join("-")}`;
    }
  }, [currentColors, loading]);

  return (
    <Grid
      container
      spacing={5}
      direction="row"
      style={{ padding: 15 }}
      alignItems="center"
    >
      <Grid item xs={5}>
        <CoolorsLink>
          {!!colors.length
            ? "See scheme in coolors.co"
            : "Find a coolors.co color scheme and paste the URL below."}
        </CoolorsLink>
        <div>
          <textarea
            value={rawInput}
            rows={4}
            style={{ width: "100%" }}
            onChange={updateColor}
            placeholder={
              "Enter can be coolors.co URL, a list of hex codes, or an XML string copied from color.adobe.com."
            }
          ></textarea>
        </div>
      </Grid>

      <Grid item xs={4}>
        {loading ? (
          <div style={{ textAlign: "center", width: "auto" }}>
            <div>Loading</div>
            <CircularProgress color="primary" size={25} thickness={5} />
          </div>
        ) : (
          <Button
            variant="outlined"
            color="primary"
            onClick={() => setLoading(true)}
            disabled={loading || !currentColors.length}
          >
            {!loading && (
              <>
                Click here to load color scheme
                {currentColors && !loading && previewElements}
              </>
            )}
          </Button>
        )}
      </Grid>

      <Grid item xs={3}>
        {colors.length > 0 && (
          <Button
            type="button"
            variant="text"
            color="secondary"
            fullWidth={false}
            onClick={() => {
              window.location.href = "?";
              return true;
            }}
          >
            Clear Current Color Scheme
          </Button>
        )}
      </Grid>
    </Grid>
  );
};

export { ColorSchemeImporter };
