import {
  FormControl,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  Tab,
  Tabs
} from "@material-ui/core";
import Fade from "@material-ui/core/Fade";
import React, { useState } from "react";
import {
  ColorChangeHandler,
  SketchPicker,
  SliderPicker,
  SwatchesPicker
} from "react-color";
import { useDispatch, useSelector } from "react-redux";
import { TabPanel } from "../../components/App";
import { ColorBlock } from "./BackgroundPreview";
import Color from "./color";
import {
  selectColors,
  selectEditIndex,
  setColor,
  setEditColorIndex
} from "./colorSlice";

export default (props = {}) => {
  const editingIdx = useSelector(selectEditIndex);
  const colors = useSelector(selectColors);
  const dispatch = useDispatch();
  const handleColorChange: ColorChangeHandler = color => {
    dispatch(setColor({ color: Color.fromHex(color.hex), index: editingIdx }));
  };
  const handleEditIdxChange = (idx: number) => dispatch(setEditColorIndex(idx));
  const [tabIdx, setTabIdx] = useState(0);
  // TODO: Fix, this case falls through the material ui accordian for some reason
  if (editingIdx < 0) {
    return null;
  }

  const handleTabChange = (_: React.ChangeEvent<{}>, newValue: number) => {
    setTabIdx(newValue);
  };

  return (
    <>
      <Grid container direction="column" style={{ alignItems: "center" }}>
        <Grid item style={{ margin: "20px 0" }}>
          <FormControl component="fieldset">
            {/* <FormLabel component="legend">Choose color to edit</FormLabel> */}
            <RadioGroup
              aria-label="position"
              name="position"
              value={editingIdx}
              onChange={(_, v) => handleEditIdxChange(+v)}
              row
            >
              {colors.map((c, i) => (
                <FormControlLabel
                  style={{ margin: 0 }}
                  key={i}
                  value={i}
                  control={<Radio color="primary" />}
                  label={<ColorBlock blockColor={c} size={15} />}
                  labelPlacement="top"
                />
              ))}
            </RadioGroup>
          </FormControl>
        </Grid>
        <Fade in={editingIdx >= 0} mountOnEnter unmountOnExit>
          <>
            <Tabs
              value={tabIdx}
              onChange={handleTabChange}
              aria-label="Color picker type"
            >
              <Tab label="Picker" id="colorpicker-tab-0" />
              <Tab label="Swatches" id="colorpicker-tab-1" />
            </Tabs>
            <TabPanel prefix="colorpicker" value={tabIdx} index={0}>
              <div className="noselect">
                <SketchPicker
                  width="90%"
                  disableAlpha={true}
                  color={colors[editingIdx].toString()}
                  onChangeComplete={handleColorChange}
                ></SketchPicker>
                <SliderPicker
                  color={colors[editingIdx].toString()}
                  onChangeComplete={handleColorChange}
                ></SliderPicker>
              </div>
            </TabPanel>

            <TabPanel prefix="colorpicker" value={tabIdx} index={1}>
              <SwatchesPicker
                color={colors[editingIdx].toString()}
                onChangeComplete={handleColorChange}
                width={350}
                height={600}
              />
            </TabPanel>
          </>
        </Fade>
      </Grid>
    </>
  );
};
