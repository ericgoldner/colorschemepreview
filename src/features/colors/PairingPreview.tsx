import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  createStyles,
  Divider,
  Fade,
  Grid,
  Theme,
  Tooltip,
  Typography
} from "@material-ui/core";
import withStyles, { WithStyles } from "@material-ui/styles/withStyles";
import React from "react";
import { connect, shallowEqual } from "react-redux";
import { AppState } from "../../store/rootReducer";
import { A11yLevel, calculateA11yByElement } from "../filters/a11y";
import {
  ElementFilterType,
  FilterResults,
  makeSelectPairingVisibility
} from "../filters/filterSlice";
import { Icons, ColorBlock } from "./BackgroundPreview";
import { ColorPairing } from "./color";
import { makeSelectColorPair, setEditColorIndex } from "./colorSlice";

const colorBlockSize = 20;
const stylesDeclaration = (theme: Theme) =>
  createStyles({
    root: {
      //minWidth: 275,
      margin: 3
    },
    title: {
      textAlign: "center",
      fontSize: "2.5em"
    },
    icons: {
      textAlign: "center"
    },
    btnContainer: {
      backgroundColor: "white",
      textAlign: "center"
    },
    heading: {},
    colorInfo: {
      "& span": {
        // lineHeight: colorBlockSize,
        fontSize: "14px",
        fontFamily: "courier, monospace",
        fontWeight: "bold"
      },
      padding: 4,
      color: "black",
      backgroundColor: "white"
    },
    editColorButton: {
      border: "1px solid black",
      display: "inline-block",
      margin: "0 0 4px 0"
    },
    colorBlock: {
      verticalAlign: "middle"
    }
  });
const stylesInjector = withStyles(stylesDeclaration);

export interface PairingPreviewProps {
  fgIdx: number;
  bgIdx: number;
  collapseColumns?: boolean;
}

export interface ConnectedProps {
  colors: ColorPairing;
  visibilities: FilterResults;
  setEditColorIndex: (idx: number) => void;
}

export interface StyledProps extends WithStyles<typeof stylesDeclaration> {}

export type ConnectedStyledPairingPreviewProps = ConnectedProps &
  StyledProps &
  PairingPreviewProps;

class PairingPreview extends React.Component<
  ConnectedStyledPairingPreviewProps,
  {}
> {
  constructor(props: ConnectedStyledPairingPreviewProps) {
    super(props);

    this.createColorStats = this.createColorStats.bind(this);
    this.getLevel = this.getLevel.bind(this);
    this.editFg = this.editFg.bind(this);
    this.editBg = this.editBg.bind(this);
  }

  private editFg() {
    this.props.setEditColorIndex(this.props.fgIdx);
  }
  private editBg() {
    this.props.setEditColorIndex(this.props.bgIdx);
  }

  shouldComponentUpdate(
    nextProps: Readonly<ConnectedStyledPairingPreviewProps>
  ): boolean {
    return (
      this.props.collapseColumns !== nextProps.collapseColumns ||
      !this.props.colors.equals(nextProps.colors) ||
      !shallowEqual(
        this.props.visibilities.elementVisibilities,
        nextProps.visibilities.elementVisibilities
      ) ||
      this.props.visibilities.pairingVisible !==
        nextProps.visibilities.pairingVisible
    );
  }

  private static textColorForRating(level: A11yLevel) {
    return level === A11yLevel.fail
      ? "#af1a1a"
      : level === A11yLevel.aa
      ? "#615a12"
      : level === A11yLevel.aaa
      ? "#2a6020"
      : "black";
  }

  private getLevel(item: ElementFilterType) {
    return calculateA11yByElement(item, this.props.colors.contrast);
  }

  createColorStats() {
    const { colors, classes } = this.props;
    const largeLvl = this.getLevel("header");
    const smalLvl = this.getLevel("paragraph");
    return (
      <Grid
        container
        direction="row"
        style={{ justifyContent: "space-around", textAlign: "center" }}
      >
        <Grid item xs={6}>
          {/* <Button
            size="small"
            className={classes.editColorButton}
            onClick={this.editFg}
            style={colors.foreground.pairAsBackground().getCssStyle()}
          >
            {colors.foreground.toString()}
          </Button> */}

          <Button
            onClick={this.editFg}
            startIcon={
              <ColorBlock
                className={`${classes.colorBlock}`}
                blockColor={colors.foreground}
                size={colorBlockSize}
              />
            }
          >
            {colors.foreground.hex()}
          </Button>
        </Grid>
        <Grid item xs={6}>
          {/* <Button
            size="small"
            className={classes.editColorButton}
            onClick={this.editBg}
            style={colors.background.pairAsBackground().getCssStyle()}
          >
            {colors.background.toString()}
          </Button> */}

          <Button
            onClick={this.editBg}
            startIcon={
              <ColorBlock
                className={`${classes.colorBlock}`}
                blockColor={colors.background}
                size={colorBlockSize}
              />
            }
          >
            {colors.background.hex()}
          </Button>
        </Grid>

        <Grid item>
          <span>
            CR:<b>{colors.contrast.toFixed(1)}</b>
          </span>
        </Grid>
        <Divider orientation="vertical" flexItem />
        <Grid item>
          <Tooltip
            title="Large Text WCAG Accessibility Level"
            aria-label="Large Text WCAG Accessibility Level"
            arrow
            placement="bottom-end"
          >
            <span
              style={{
                margin: 3,
                fontSize: "1.1em",
                color: PairingPreview.textColorForRating(largeLvl)
              }}
            >
              {A11yLevel[largeLvl]}
            </span>
          </Tooltip>
        </Grid>
        <Divider orientation="vertical" flexItem />
        <Grid item>
          <Tooltip
            title="Small Text WCAG Accessibility Level"
            aria-label="Small Text WCAG Accessibility Level"
            arrow
            placement="bottom-end"
          >
            <span style={{ color: PairingPreview.textColorForRating(smalLvl) }}>
              {A11yLevel[smalLvl]}
            </span>
          </Tooltip>
        </Grid>
      </Grid>
    );
  }

  render() {
    const {
      header,
      buttons,
      colorInfo,
      icons,
      paragraph
    } = this.props.visibilities.elementVisibilities;
    const { colors, classes, collapseColumns } = this.props;

    return (
      <Fade
        in={this.props.visibilities.pairingVisible}
        unmountOnExit={!!collapseColumns}
        mountOnEnter={!!collapseColumns}
      >
        <Card
          className={`${classes.root}`}
          variant="elevation"
          style={{ ...colors.getCssStyle() }}
        >
          <Collapse in={colorInfo} unmountOnExit mountOnEnter>
            <CardHeader
              className={classes.colorInfo}
              title={this.createColorStats()}
            ></CardHeader>
          </Collapse>

          <Fade in={header || icons || paragraph}>
            <CardContent>
              <Collapse in={header} unmountOnExit mountOnEnter>
                {/* <A11yBadge level={getLevel("header")}> */}
                <Typography className={classes.title} variant="h3" gutterBottom>
                  Header
                </Typography>
                {/* </A11yBadge> */}
              </Collapse>

              <Collapse in={icons} unmountOnExit mountOnEnter>
                <Typography className={classes.icons}>
                  <Icons color={colors.foreground} />
                </Typography>
              </Collapse>

              <Collapse in={paragraph} unmountOnExit mountOnEnter>
                {/* <A11yBadge level={getLevel("paragraph")}> */}
                <Typography variant="body2" component="p">
                  <span>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea
                    consectetur sit, adipisci hic similique molestiae!
                  </span>
                </Typography>
                {/* </A11yBadge> */}
              </Collapse>
            </CardContent>
          </Fade>
          <Collapse in={buttons} unmountOnExit mountOnEnter>
            <CardActions className={classes.btnContainer}>
              <Button
                variant="contained"
                style={new ColorPairing(
                  colors.background,
                  colors.foreground
                ).getCssStyle()}
                size="small"
              >
                Button
              </Button>
              <Button
                variant="contained"
                style={colors.getCssStyle()}
                size="small"
              >
                Button
              </Button>
            </CardActions>
          </Collapse>
        </Card>
      </Fade>
    );
  }
}

const makeMapStateToProps = () => {
  const colorSel = makeSelectColorPair();
  const filterSel = makeSelectPairingVisibility();
  const mapStateToProps = (state: AppState, props: PairingPreviewProps) => ({
    colors: colorSel(state, props.fgIdx, props.bgIdx),
    visibilities: filterSel(state, props.fgIdx, props.bgIdx)
  });
  return mapStateToProps;
};

const mapDispatchToProps = {
  setEditColorIndex
};

export default connect(
  makeMapStateToProps,
  mapDispatchToProps
)(stylesInjector(PairingPreview));
