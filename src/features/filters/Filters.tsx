import {
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Grid,
  Switch,
  makeStyles,
  Button
} from "@material-ui/core";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { typedKeys } from "../../utils";
import { FilterColorLayers } from "./ColorFilters";
import "./Filters.css";
import {
  A11yFilterState,
  ElementFilterType,
  selectA11yFilters,
  selectElementFilters,
  setA11yFilter,
  setElementFilter,
  resetFilters
} from "./filterSlice";

export type CategoryKeyFilterProps<TLabel> = {
  label: TLabel;
  onChanged: (label: TLabel, value: boolean) => void;
  checked: boolean;
};

export function CategoryKeyFilter<T>({
  label,
  onChanged,
  checked
}: CategoryKeyFilterProps<T>) {
  return (
    <label>
      <input
        type="checkbox"
        checked={checked}
        onChange={e => onChanged(label, e.target.checked)}
      />
      {label}
    </label>
  );
}

export const FilterElements: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const filter = useSelector(selectElementFilters);

  const filterChanged = (item: ElementFilterType, visible: boolean) => {
    dispatch(setElementFilter({ item, visible }));
  };

  const createLabel = (str: string) =>
    str
      .replace(/[-_A-Z]/g, " $&")
      .replace(/[\b[a-z]/g, l => l.toUpperCase())
      .trim();

  // The order of elements as they're displayed to the user.
  const orderedKeys: ElementFilterType[] = [
    "colorInfo",
    "header",
    "icons",
    "paragraph",
    "buttons"
  ];

  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">Element Filter</FormLabel>
      <FormGroup>
        {orderedKeys.map(item => (
          <FormControlLabel
            key={item}
            checked={filter[item]}
            control={
              <Switch
                color="primary"
                onChange={(_, checked) => filterChanged(item, checked)}
              />
            }
            label={createLabel(item)}
          />
        ))}
      </FormGroup>
      {/* <FormHelperText> Helper text here. </FormHelperText> */}
    </FormControl>
  );
};

export const FilterA11y: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const filter = useSelector(selectA11yFilters);

  const filterChanged = (item: keyof A11yFilterState, visible: boolean) => {
    dispatch(setA11yFilter({ item, visible }));
  };

  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">Accessibility Filter</FormLabel>
      <FormGroup>
        {typedKeys(filter).map(item => (
          <FormControlLabel
            key={item}
            checked={filter[item]}
            control={
              <Switch
                color="primary"
                onChange={(_, checked) => filterChanged(item, checked)}
              />
            }
            label={item.toUpperCase()}
          />
        ))}
      </FormGroup>
      {/* <FormHelperText> Helper text here. </FormHelperText> */}
    </FormControl>
  );
};

const filterStyles = makeStyles(theme => ({
  filterContainer: {
    textAlign: "center",
    "& > *": {
      textAlign: "center",
      marginBottom: 20
    }
  },
  looseFilterContainer: {
    display: "flex",
    flexDirection: "column",
    padding: 10,
    margin: 10
  }
}));

export const Filters: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const handleResetFilters = () => {
    dispatch(resetFilters());
  };
  const classes = filterStyles();
  return (
    <>
      <div className={classes.looseFilterContainer}>
        <Button
          onClick={handleResetFilters}
          variant="contained"
          color="secondary"
        >
          Reset Filters
        </Button>
      </div>
      <Grid container className={classes.filterContainer}>
        <Grid item xs={6}>
          <FilterColorLayers type="fg" label="Foreground" />
        </Grid>
        <Grid item xs={6}>
          <FilterColorLayers type="bg" label="Background" />
        </Grid>
        <Grid item xs={6}>
          <FilterElements />
        </Grid>
        <Grid item xs={6}>
          <FilterA11y />
        </Grid>
      </Grid>
    </>
  );
};
