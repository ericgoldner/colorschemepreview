import Color from "../colors/color";
import {
  FormControlLabel,
  FormGroup,
  FormLabel,
  FormControl
} from "@material-ui/core";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  setColorFilter,
  ColorFilterType,
  selectColorFilters
} from "./filterSlice";
import { ColoredSwitch } from "../colors/ColoredSwitch";
import { selectColors } from "../colors/colorSlice";

export interface FilterColorsProps {
  colors: Color[];
  fg?: Color;
  bg?: Color;
  type: ColorFilterType;
}

export interface FilterLayerColorsProps {
  label: string;
  type: ColorFilterType;
}

export const FilterColorLayers: React.FC<FilterLayerColorsProps> = ({
  type,
  label
}) => {
  const colorFilters = useSelector(selectColorFilters);
  const colors = useSelector(selectColors);
  const dispatch = useDispatch();

  return (
    <FilterThemeColors
      header={label}
      colors={colors}
      onChange={(index, visible) =>
        dispatch(setColorFilter({ item: type, index, visible }))
      }
      visibilities={colorFilters[type]}
    />
  );
};

export interface FilterThemeColorsProps {
  header: string;
  colors: Color[];
  visibilities: boolean[];
  onChange: (index: number, value: boolean) => void;
}
export const FilterThemeColors: React.FC<FilterThemeColorsProps> = ({
  header,
  colors,
  visibilities,
  onChange
}) => {
  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">{header}</FormLabel>
      <FormGroup>
        {colors.map((c, i) => (
          <FormControlLabel
            key={i}
            control={
              <ColoredSwitch
                checked={visibilities[i]}
                customColor={c}
                onChange={e => onChange(i, e.target.checked)}
              />
            }
            label={c.toString()}
          />
        ))}
      </FormGroup>
    </FormControl>
  );
};
