import filterSlice, {
  FilterState,
  setA11yFilter,
  setElementFilter,
  SetElementFilterPayload,
  setColorFilters,
  SetColorFiltersPayload,
  SetColorFilterPayload,
  setColorFilter
} from "./filterSlice";
import produce from "immer";

const initialState: FilterState = {
  colors: {
    fg: [],
    bg: []
  },
  elements: {
    colorInfo: true,
    buttons: true,
    header: true,
    icons: true,
    paragraph: true
  },
  a11yLevels: {
    fail: true,
    aa: true,
    aaa: true
  }
};

describe("filterSlice", () => {
  it("should handle initial state", () => {
    expect(filterSlice(undefined, { type: "" })).toEqual(initialState);
  });

  it("should handle setA11yFilter", () => {
    expect(
      filterSlice(initialState, {
        type: setA11yFilter.type,
        payload: {
          item: "fail",
          visible: false
        }
      })
    ).toEqual(
      produce(initialState, draftState => {
        draftState.a11yLevels.fail = false;
      })
    );
  });

  it("should handle setItem filter", () => {
    expect(
      filterSlice(initialState, {
        type: setElementFilter.type,
        payload: {
          key: "header",
          visible: false
        } as SetElementFilterPayload
      })
    ).toEqual(
      produce(initialState, draftState => {
        draftState.elements.header = false;
      })
    );
  });

  it("should handle setColorFilters action", () => {
    expect(
      filterSlice(initialState, {
        type: setColorFilters.type,
        payload: {
          visible: [true, true, true]
        } as SetColorFiltersPayload
      })
    ).toEqual(
      produce(initialState, draftState => {
        draftState.colors = {
          fg: [true, true, true],
          bg: [true, true, true]
        };
      })
    );
  });

  it("should handle setColorFilter action", () => {
    expect(
      (() => {
        const currentState = produce(initialState, draftState => {
          draftState.colors = {
            fg: [true, true, true],
            bg: [true, true, true]
          };
        });

        return filterSlice(currentState, {
          type: setColorFilter.type,
          payload: {
            index: 0,
            item: "fg",
            visible: false
          } as SetColorFilterPayload
        });
      })()
    ).toEqual(
      produce(initialState, draftState => {
        draftState.colors = {
          fg: [false, true, true],
          bg: [true, true, true]
        };
      })
    );
  });
});
