import { ElementFilterState } from "./filterSlice";

export enum A11yType {
  largeText,
  smallText
}

export enum A11yLevel {
  fail,
  aa,
  aaa,
  none
}

export interface A11yCalculator {
  (contrast: number): A11yLevel;
}

export const createA11yCalculator = (
  aaThreshold: number,
  aaaThreshold: number
): A11yCalculator => {
  return (contrast: number) => {
    const thresholds = [
      {
        threshold: aaaThreshold,
        result: A11yLevel.aaa
      },
      {
        threshold: aaThreshold,
        result: A11yLevel.aa
      },
      {
        threshold: 0,
        result: A11yLevel.fail
      }
    ];
    return (
      thresholds.find(t => contrast >= t.threshold)?.result ?? A11yLevel.none
    );
  };
};

export const smallTextA11yCalculator = createA11yCalculator(4.5, 7);
export const largeTextA11yCalculator = createA11yCalculator(3, 4.5);

export const calculateA11yByElement = (
  type: keyof ElementFilterState,
  contrast: number
): A11yLevel => {
  switch (type) {
    case "buttons":
    case "paragraph":
    case "icons":
      return smallTextA11yCalculator(contrast);

    case "header":
      return largeTextA11yCalculator(contrast);

    default:
      return A11yLevel.none;
  }
};
