import { createSlice, PayloadAction, createSelector } from "@reduxjs/toolkit";
import { produce } from "immer";
import { AppState } from "../../store/rootReducer";
import { selectColors } from "../colors/colorSlice";
import { calculateA11yByElement, A11yLevel } from "./a11y";
import Color from "../colors/color";
import { typedKeys, filledArray, generatedArray } from "../../utils";

// STATE

export interface ElementFilterState {
  colorInfo: boolean;
  header: boolean;
  icons: boolean;
  paragraph: boolean;
  buttons: boolean;
}
export type ElementFilterType = keyof ElementFilterState;
export interface A11yFilterState {
  aaa: boolean;
  aa: boolean;
  fail: boolean;
}
export interface ColorFilterState {
  fg: boolean[];
  bg: boolean[];
}
export interface FilterState {
  colors: ColorFilterState;
  elements: ElementFilterState;
  a11yLevels: A11yFilterState;
}

export type ColorFilterType = keyof ColorFilterState;

export const initialState: FilterState = {
  colors: {
    fg: [],
    bg: []
  },
  elements: {
    colorInfo: true,
    buttons: true,
    header: true,
    icons: true,
    paragraph: true
  },
  a11yLevels: {
    fail: true,
    aa: true,
    aaa: true
  }
};
// END STATE

// SELECTORS

export const selectFilters = (state: AppState) => state.filters;
export const selectColorFilters = (state: AppState) =>
  selectFilters(state).colors;
export const selectA11yFilters = (state: AppState) =>
  selectFilters(state).a11yLevels;
export const selectElementFilters = (state: AppState) =>
  selectFilters(state).elements;

export interface FilterResults {
  elementVisibilities: ElementFilterState;
  pairingVisible: boolean;
}

enum VisibilityLevel {
  visible,
  hidden,
  collapsable
}
type ElementVisibilityLevels = { [key in ElementFilterType]: VisibilityLevel };

/**
 * Generates the results of visibility filters.
 * Rows are backgrounds, columns are foregrounds
 */
export const selectFieldVisibilities = createSelector(
  selectColors,
  selectFilters,
  (colors: Color[], filters: FilterState) => {
    const len = colors.length;

    // Filters by element state right off the bat.
    const someElsVisible = Object.values(filters.elements).includes(true);
    const createFilterResults = (): FilterResults => ({
      elementVisibilities: Object.assign({}, filters.elements),
      pairingVisible: someElsVisible
    });
    const createListOfElResults = () =>
      generatedArray<FilterResults>(createFilterResults, len);

    const results: FilterResults[][] = generatedArray<FilterResults[]>(
      createListOfElResults,
      len
    );

    // Filter by contrast, symmetric along identity
    const visByLevel = (lvl: A11yLevel) => {
      if (lvl === A11yLevel.none) return VisibilityLevel.collapsable;
      return (filters.a11yLevels.aaa && lvl === A11yLevel.aaa) ||
        (filters.a11yLevels.aa && lvl === A11yLevel.aa) ||
        (filters.a11yLevels.fail && lvl === A11yLevel.fail)
        ? VisibilityLevel.visible
        : VisibilityLevel.hidden;
    };

    const visByElementLvl = (type: ElementFilterType, contrast: number) =>
      visByLevel(calculateA11yByElement(type, contrast));

    for (let bgIdx = 0; bgIdx < len; bgIdx++) {
      // Color on itself is hidden
      results[bgIdx][bgIdx].pairingVisible = false;

      for (let fgIdx = 0; fgIdx < bgIdx; fgIdx++) {
        const fg = colors[fgIdx];
        const bg = colors[bgIdx];
        const contrast = fg.contrastWith(bg);

        const elVisibilities = typedKeys(
          results[bgIdx][fgIdx].elementVisibilities
        ).reduce((acc, k) => {
          acc[k] = visByElementLvl(k, contrast);
          if (acc[k] === VisibilityLevel.hidden) {
            results[bgIdx][fgIdx].elementVisibilities[k] = false;
            results[fgIdx][bgIdx].elementVisibilities[k] = false;
          }
          return acc;
        }, {} as Partial<ElementVisibilityLevels>) as ElementVisibilityLevels;

        // Collapse pairing if all elements are invisible
        if (
          !typedKeys(elVisibilities).some((k, i) => {
            return (
              // Filter items that are hidden  based on contrast or element type
              [
                VisibilityLevel.visible /*, VisibilityLevel.collapsable*/
              ].includes(elVisibilities[k]) &&
              results[bgIdx][fgIdx].elementVisibilities[k]
            );
          })
        ) {
          results[bgIdx][fgIdx].pairingVisible = false;
          results[fgIdx][bgIdx].pairingVisible = false;
        }
      }
    }

    // Filter by color
    filters.colors.bg.forEach((v, i) => {
      if (!v) results[i].forEach(c => (c.pairingVisible = false));
    });
    filters.colors.fg.forEach((v, i) => {
      if (!v) results.forEach(c => (c[i].pairingVisible = false));
    });

    return results;
  }
);

const selectFg = (_: AppState, fg: number) => fg;
const selectBg = (_: AppState, _2: number, bg: number) => bg;
export const makeSelectPairingVisibility = () =>
  createSelector(
    [selectFieldVisibilities, selectFg, selectBg],
    (results: FilterResults[][], fg: number, bg: number) => results[bg][fg]
  );

/** Evaluates whether any colors of specified bg are visible */

export const selectBgColorVisibilities = createSelector(
  [selectFieldVisibilities],
  (results: FilterResults[][]) => {
    return results.map(r => r.some(r2 => r2.pairingVisible));
  }
);

//

type SetFilterPayload<TParent, TResult = boolean> = {
  item: keyof TParent;
  visible: TResult;
};

export type SetElementFilterPayload = SetFilterPayload<ElementFilterState>;
export type SetA11yFilterPayload = SetFilterPayload<A11yFilterState>;
export type SetColorFiltersPayload = {
  item?: keyof ColorFilterState;
  visible: boolean | boolean[];
};
export type SetColorFilterPayload = SetFilterPayload<ColorFilterState> & {
  index: number;
};

const filterSlice = createSlice({
  name: "filters",
  initialState,
  reducers: {
    setElementFilter: (
      state: FilterState,
      action: PayloadAction<SetElementFilterPayload>
    ) => {
      const { item, visible } = action.payload;
      state.elements[item] = visible;
    },

    setA11yFilter: (
      state: FilterState,
      action: PayloadAction<SetA11yFilterPayload>
    ) => {
      const { item, visible } = action.payload;
      state.a11yLevels[item] = visible;
    },

    setColorFilter: (
      state: FilterState,
      action: PayloadAction<SetColorFilterPayload>
    ) => {
      const { item, index, visible } = action.payload;
      state.colors[item][index] = visible;
    },

    setColorFilters: (
      state: FilterState,
      action: PayloadAction<SetColorFiltersPayload>
    ) => {
      const { item, visible } = action.payload;
      const arraysToEdit = [];
      (!item || item === "fg") && arraysToEdit.push(state.colors.fg);
      (!item || item === "bg") && arraysToEdit.push(state.colors.bg);

      arraysToEdit.forEach(a => {
        if (typeof visible === "boolean") {
          a.fill(visible);
        } else {
          a.splice(0, a.length, ...visible);
        }
      });
    },

    resetFilters: (prevState: FilterState) => {
      const numColors = prevState.colors.fg.length;
      return produce(initialState, oldInitalState => {
        oldInitalState.colors.fg = Array(numColors).fill(true);
        oldInitalState.colors.bg = Array(numColors).fill(true);
      });
    }
  },
  extraReducers: {
    setColors: (state: FilterState, action: PayloadAction<Color[]>) => {
      // Reset the filters if an entirely new set of colors have been addded.
      const numColors = action.payload.length;
      state.colors.fg = filledArray(true, numColors);
      state.colors.bg = state.colors.fg.slice();
    }
  }
});

export const {
  setElementFilter,
  setA11yFilter,
  setColorFilter,
  setColorFilters,
  resetFilters
} = filterSlice.actions;

export default filterSlice.reducer;
