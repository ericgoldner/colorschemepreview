import {
  AppBar,
  Box,
  makeStyles,
  Tab,
  Tabs,
  Typography,
  createStyles
} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import "axios";
import React, { useState } from "react";
import { EditablePreview } from "../features/editablePreview/EditablePreview";
import "./App.css";
import { ColorSchemePreviewPage } from "./ColorPage";

export interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  prefix: string;
  value: any;
}

export function TabPanel(props: TabPanelProps) {
  const { children, value, index, prefix, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`${prefix}-tabpanel-${index}`}
      aria-labelledby={`${prefix}-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

/** From: https://material-ui.com/components/drawers/ */
const drawerWidth = 400;
const appBarHeight = 60;
export const useAppStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: "flex"
    },
    appBarSpacer: {
      marginTop: appBarHeight
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      height: appBarHeight
    },
    heading: {
      fontSize: "1.5em",
      fontWeight: "bold"
    },
    appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    hide: {
      display: "none"
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0
    },
    drawerPaper: {
      marginTop: appBarHeight,
      height: `calc(100% - ${appBarHeight}px)`,
      width: drawerWidth
    },
    drawerHeader: {
      display: "flex",
      alignItems: "center",
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
      justifyContent: "flex-end"
    },
    content: {
      flexGrow: 1,
      padding: 0,
      paddingTop: appBarHeight,
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      marginLeft: 0
    },
    contentShift: {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      }),
      marginLeft: drawerWidth
    },
    tabPanel: {
      padding: 0,
      margin: 0,
      flexDirection: "column"
    }
  })
);

export const App: React.FC<{}> = () => {
  const classes = useAppStyles();
  const [tabIdx, setTabIdx] = useState(0);

  const handleTabChange = (event: React.ChangeEvent<{}>, tabIdx: number) => {
    setTabIdx(tabIdx);
  };

  // if (editedColorIdx >= 0 && !drawerStatus.drawer) {
  //   toggleDrawerPanel("drawer", true);
  // }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <main
        className={`App ${classes.content}`}
        // className={`App ${classes.content} ${drawerStatus.drawer &&
        //   classes.contentShift}`}
      >
        <AppBar position="fixed" className={classes.appBar}>
          <Tabs
            value={tabIdx}
            onChange={handleTabChange}
            aria-label="Color scheme preview type"
          >
            <Tab id="page-tab-0" label="Color Comparison" />
            <Tab id="page-tab-1" label="Customizable Page Example" />
          </Tabs>
        </AppBar>
        {/* <div className={classes.appBarSpace}> </div> */}
        <TabPanel prefix="page" value={tabIdx} index={0}>
          <ColorSchemePreviewPage />
        </TabPanel>
        <TabPanel prefix="page" value={tabIdx} index={1}>
          <EditablePreview />
        </TabPanel>
      </main>
    </div>
  );
};
