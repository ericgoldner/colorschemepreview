import {
  DrawerProps,
  useTheme,
  Drawer,
  IconButton,
  ExpansionPanel,
  ExpansionPanelSummary,
  Typography,
  ExpansionPanelDetails,
  Grid,
  Switch,
  Fab
} from "@material-ui/core";

import { useDispatch, useSelector } from "react-redux";
import TuneIcon from "@material-ui/icons/Tune";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {
  setEditColorIndex,
  selectNumColors,
  selectEditIndex
} from "../features/colors/colorSlice";

import React, { useState, useRef } from "react";

import ColorEditorPanel from "../features/colors/ColorEditorPanel";

import { Filters } from "../features/filters/Filters";

import { ColorSchemeImporter } from "../features/colors/ColorSchemeImporter";

import { ColorSchemePreview } from "../features/colors/BackgroundPreview";
import { useAppStyles } from "./App";

interface ColorComparisonDrawerProps {
  //colors: boolean;
  filters: boolean;
  drawer: boolean;
}

const ColorComparisonDrawer = ({
  drawerStatus,
  numColors,
  changeDrawerStatus
}: {
  drawerStatus: ColorComparisonDrawerProps;
  numColors: number;
  changeDrawerStatus: (
    status: keyof ColorComparisonDrawerProps,
    visible: boolean
  ) => void;
} & DrawerProps) => {
  const theme = useTheme();
  const classes = useAppStyles();
  const dispatch = useDispatch();
  const editedColorIdx = useSelector(selectEditIndex);
  return (
    <Drawer
      anchor="left"
      variant="persistent"
      className={classes.drawer}
      open={drawerStatus.drawer || editedColorIdx >= 0}
      onClose={() => changeDrawerStatus("drawer", false)}
      classes={{
        paper: classes.drawerPaper
      }}
    >
      <div className={classes.drawerHeader}>
        <h2>Edit Colors &amp; Filters</h2>
        <IconButton onClick={() => changeDrawerStatus("drawer", false)}>
          {theme.direction === "ltr" ? (
            <ChevronLeftIcon />
          ) : (
            <ChevronRightIcon />
          )}
        </IconButton>
      </div>
      <ExpansionPanel
        TransitionProps={{ unmountOnExit: true }}
        expanded={editedColorIdx >= 0}
        disabled={!numColors}
        onChange={(_, colors) => {
          dispatch(setEditColorIndex(colors ? 0 : -1));
        }}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel-cp1-content"
          id="panel-cp1-header"
        >
          <Typography className={classes.heading}>Edit Colors</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails
          className={`${classes.tabPanel}`}
          id="panel-cp1-content"
        >
          <ColorEditorPanel />
        </ExpansionPanelDetails>
      </ExpansionPanel>

      <ExpansionPanel
        TransitionProps={{ unmountOnExit: true }}
        expanded={drawerStatus.filters}
        disabled={!numColors}
        onChange={(_, filters) => changeDrawerStatus("filters", filters)}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel-cp2-content"
          id="panel-cp2-header"
        >
          <Typography className={classes.heading}>Filters</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails
          className={`${classes.tabPanel}`}
          id="panel-cp2-content"
        >
          <Filters />
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </Drawer>
  );
};

export const ColorSchemePreviewPage: React.FC<{}> = () => {
  const classes = useAppStyles();
  const [drawerStatus, setDrawerStatus] = useState<ColorComparisonDrawerProps>({
    filters: true,
    drawer: false
  });

  const editIdx = useSelector(selectEditIndex);
  const [collapseCols, setCollapseCols] = useState(false);
  const numColors = useSelector(selectNumColors);
  const dispatch = useDispatch();
  const containerRef = useRef<HTMLDivElement>(null);
  const handleEditingIdxChange = (index: number) => {
    dispatch(setEditColorIndex(index));
  };

  const toggleDrawerPanel = (
    key: keyof ColorComparisonDrawerProps,
    val: boolean
  ) => {
    if (key === "drawer" && !val) {
      // Automatically set the color index if closing the panel
      handleEditingIdxChange(-1);
    }
    setDrawerStatus({ ...drawerStatus, [key]: val });
  };

  if (editIdx >= 0 && !drawerStatus.drawer) {
    setDrawerStatus({ ...drawerStatus, drawer: true });
  }

  return (
    <div
      ref={containerRef}
      className={`${drawerStatus.drawer && classes.contentShift}`}
    >
      <ColorComparisonDrawer
        changeDrawerStatus={toggleDrawerPanel}
        numColors={numColors}
        drawerStatus={drawerStatus}
      />

      <ColorSchemeImporter />

      {numColors > 0 && (
        <Grid container direction="row" alignItems="center">
          <Grid item>
            <DrawerToggler
              onToggle={() => toggleDrawerPanel("drawer", !drawerStatus.drawer)}
            />
          </Grid>
          <Grid item>
            <label>
              <Switch
                onChange={() => setCollapseCols(collapse => !collapse)}
                value={collapseCols}
              />
              Collapse Columns?
            </label>
          </Grid>
        </Grid>
      )}
      <ColorSchemePreview collapseCols={collapseCols} />
    </div>
  );
};

const DrawerToggler = ({ onToggle: toggled }: { onToggle: () => void }) => {
  return (
    <Fab
      variant="extended"
      color="primary"
      style={{ margin: 8 }}
      onClick={toggled}
    >
      <TuneIcon fontSize="default" />
      Colors and Filters
    </Fab>
  );
};
