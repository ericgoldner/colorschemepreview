import { combineReducers, Action } from "redux";
import colorReducer from "../features/colors/colorSlice";
import filterReducer from "../features/filters/filterSlice";
import { ThunkAction } from "@reduxjs/toolkit";

const rootReducer = combineReducers({
  colors: colorReducer,
  filters: filterReducer
});

export type AppState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, AppState, unknown, Action<string>>;

export default rootReducer;
