import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./rootReducer";
import Color from "../features/colors/color";
import { produce } from "immer";
import {
  normalizeColorScheme,
  parseScheme
} from "../features/colors/colorScheme";
import { getQueryStringParam } from "../utils";

/** Colors that augment any scheme, e.g. white and black */
const defaultColors = [Color.white, Color.black];
/**
 * The scheme to use by default if none is provided
 * (so users can get a sense of the website.)
 */
const defaultScheme = "ff6978-fffcf9-b1ede8-6d435a-352d39"
  .split("-")
  .map(Color.fromHex);

const getPreloadedState = () => {
  const requestedText = getQueryStringParam("colors") || "";
  let requestedColors = parseScheme(requestedText) ?? {
    source: "default",
    colors: defaultScheme
  };

  // Load the full state first, so we can edit only some members of some members.
  const initialState = rootReducer(undefined, { type: "" });
  return produce(initialState, oldState => {
    const colors = normalizeColorScheme(requestedColors.colors, defaultColors);
    const visibilities = colors.map(_ => true);
    oldState.colors.colors = colors.map(c => c.hex());
    oldState.colors.editingIdx = 0;
    oldState.filters.colors = {
      fg: visibilities,
      bg: visibilities
    };

    if (colors.length) {
      oldState.colors.customColors = [];
    }
  });
};

export default configureStore({
  reducer: rootReducer,
  preloadedState: getPreloadedState()
});
