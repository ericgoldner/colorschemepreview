function getSearchParams() {
  // Doesn't work in IE
  const url = window.location.search;
  return new URLSearchParams(url);
}

export function getQueryStringParam(key: string) {
  return getSearchParams().get(key);
}

export function getQueryStringParams(key: string) {
  return getSearchParams().getAll(key);
}

export function typedKeys<TObj extends {}>(obj: TObj): Array<keyof TObj> {
  return Object.keys(obj) as Array<keyof TObj>;
}

export function filledArray<T>(value: T, length: number) {
  const arr: T[] = [];
  arr.length = length;
  return arr.fill(value);
}

export function generatedArray<T>(valueFn: (idx: number) => T, length: number) {
  const arr: T[] = [];
  arr.length = length;
  for (let i = 0; i < length; i++) {
    arr[i] = valueFn(i);
  }
  return arr;
}

export interface Clipboard {
  copy(data: string): Promise<void>;
  paste(): Promise<string>;
}

export const clipboard = (() => {
  const navigatorClipboard = (): Clipboard => {
    const getPermissions = () => {
      return Promise.resolve(true);
      // return navigator.permissions.query({ name: "clipboard" }).then(result => {
      //   if (result.state === "granted" || result.state === "prompt") {
      //     return true;
      //   }
      //   throw new Error("No clipboard permissions granted.");
      // });
    };

    return {
      copy(data: string) {
        return getPermissions().then(() => navigator.clipboard.writeText(data));
      },

      paste() {
        return typeof navigator.clipboard.readText === "function"
          ? getPermissions().then(() => navigator.clipboard?.readText())
          : Promise.reject("FFx or unsupported");
      }
    };
  };

  const legacyClipboard = (): Clipboard => ({
    copy(data: string) {
      if (!document)
        throw new Error("No document provided for clipboard copy.");
      // FROM: https://hackernoon.com/copying-text-to-clipboard-with-javascript-df4d4988697f

      const el = document.createElement("textarea"); // Create a <textarea> element
      el.value = data; // Set its value to the string that you want copied
      el.setAttribute("readonly", ""); // Make it readonly to be tamper-proof
      el.style.position = "absolute";
      el.style.left = "-9999px"; // Move outside the screen to make it invisible
      document.body.appendChild(el); // Append the <textarea> element to the HTML document
      const selected = document.getSelection()?.rangeCount // Check if there is any content selected previously
        ? document.getSelection()?.getRangeAt(0) ?? false // Store selection if found
        : false; // Mark as false to know no selection existed before
      el.select(); // Select the <textarea> content
      document.execCommand("copy"); // Copy - only works as a result of a user action (e.g. click events)
      document.body.removeChild(el); // Remove the <textarea> element
      if (selected) {
        // If a selection existed before copying
        document.getSelection()?.removeAllRanges(); // Unselect everything on the HTML document
        document.getSelection()?.addRange(selected); // Restore the original selection
      }
      return Promise.resolve();
    },
    paste(): Promise<string> {
      return Promise.reject(
        "Clipboard feature not available in this browser or clipboard permissions weren't granted."
      );
    }
  });

  // Prefer legacy for now due to permissions
  return navigator.clipboard ? navigatorClipboard() : legacyClipboard();
})();
